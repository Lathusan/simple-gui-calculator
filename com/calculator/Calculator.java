package com.calculator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Color;

public class Calculator extends javax.swing.JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	JTextArea jta1, jta2, jta3;

	JButton b1, b2, b3, b4, b5;

	Calculator() {

		JFrame jf = new JFrame("Sample");
		jf.setSize(400, 400);
		jf.setDefaultCloseOperation(EXIT_ON_CLOSE);
		jf.setVisible(true);
		jf.setLayout(null);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int h = dim.height;
		int w = dim.width;

		int fh = jf.getSize().height;
		int fw = jf.getSize().width;

		int x = (w - fw) / 2;
		int y = (h - fh) / 2;
		jf.setLocation(x, y);

		// JLabel jl = new JLabel();
		// jl.setText("Number 1 :");
		// jl.setBounds(100, 25, 150, 25);

		jta1 = new JTextArea("");
		jf.add(jta1);
		jta1.setBounds(20, 25, 50, 15);
		

		jta2 = new JTextArea("");
		jf.add(jta2);
		jta2.setBounds(100, 25, 50, 15);

		jta3 = new JTextArea();
		jf.add(jta3);
		jta3.setBounds(180, 25, 50, 15);

		b1 = new JButton("C");
		jf.add(b1);
		b1.setBounds(20, 50, 45, 20);
		b1.addActionListener(this);

		b2 = new JButton("+");
		jf.add(b2);
		b2.setBounds(70, 50, 45, 20);
		b2.addActionListener(this);

		b3 = new JButton("-");
		jf.add(b3);
		b3.setBounds(120, 50, 45, 20);
		b3.addActionListener(this);

		b4 = new JButton("x");
		jf.add(b4);
		b4.setBounds(170, 50, 45, 20);
		b4.addActionListener(this);

		b5 = new JButton("/");
		jf.add(b5);
		b5.setBounds(220, 50, 45, 20);
		b5.addActionListener(this);
	}
	// Font f = new Font("Ubuntu", Font.ITALIC,15);
	// jl.setFont(f);
	// jl.setBounds(50,50,50,50);
	// jl.setForeground(Color.black);
	// jf.add(jl);

	public void actionPerformed(ActionEvent ae) {

		int ta1 = Integer.parseInt(jta1.getText());
		int ta2 = Integer.parseInt(jta2.getText());
		int ta3;

		String bmsg = ae.getActionCommand();

		if (bmsg.equals("C")) {
			jta1.setText(null);
			jta2.setText(null);
			jta3.setText(null);
		}

		if (bmsg.equals("+")) {
			ta3 = ta1 + ta2;
			jta3.setText(String.valueOf(ta3));
		}

		if (bmsg.equals("-")) {
			ta3 = ta1 - ta2;
			jta3.setText(String.valueOf(ta3));
		}

		if (bmsg.equals("x")) {
			ta3 = ta1 * ta2;
			jta3.setText(String.valueOf(ta3));
		}

		if (bmsg.equals("/")) {
			ta3 = ta1 / ta2;
			jta3.setText(String.valueOf(ta3));
		}

	}

}
