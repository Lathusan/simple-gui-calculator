package com.calculator;

import javax.swing.SwingUtilities;

public class CalculatorDemo {
	
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				
				new Calculator();
			}
		});

	}

}
